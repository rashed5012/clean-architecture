﻿using CleanArchitecture.Application.Abstractions.Messaging;

namespace CleanArchitecture.Application.Bookings.RejectBooking;

public sealed record RejectBookingCommand(Guid BookingId) : ICommand;