﻿using CleanArchitecture.Application.Abstractions.Caching;
using CleanArchitecture.Application.Abstractions.Messaging;

namespace CleanArchitecture.Application.Bookings.GetBooking;

public sealed record GetBookingQuery(Guid BookingId) : ICachedQuery<BookingResponse>
{
    public string CacheKey => $"bookings-{BookingId}";
    public TimeSpan? Expiration => null;
}