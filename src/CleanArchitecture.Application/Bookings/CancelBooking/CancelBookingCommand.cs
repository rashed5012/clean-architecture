﻿using CleanArchitecture.Application.Abstractions.Messaging;

namespace CleanArchitecture.Application.Bookings.CancelBooking;

public record CancelBookingCommand(Guid BookingId) : ICommand;