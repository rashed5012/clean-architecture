﻿using CleanArchitecture.Application.Abstractions.Messaging;

namespace CleanArchitecture.Application.Bookings.ConfirmBooking;

public sealed record ConfirmBookingCommand(Guid BookingId) : ICommand;