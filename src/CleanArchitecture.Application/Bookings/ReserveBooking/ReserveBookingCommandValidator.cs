﻿using FluentValidation;

namespace CleanArchitecture.Application.Bookings.ReserveBooking;

public class ReserveBookingCommandValidator : AbstractValidator<ReserveBookingCommand>
{
    public ReserveBookingCommandValidator()
    {
        RuleFor(e => e.UserId).NotEmpty();
        RuleFor(e => e.ApartmentId).NotEmpty();
        RuleFor(e => e.StartDate).LessThan(e => e.EndDate);
    }
}