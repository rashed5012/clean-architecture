﻿namespace CleanArchitecture.Api.Controllers;

internal static class Permissions
{
    public const string UsersRead = "users:read";
}


internal static class ApiVersions
{
    public const string V1 = "1";
    public const string V2 = "2";
}
