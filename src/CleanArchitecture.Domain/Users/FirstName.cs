﻿namespace CleanArchitecture.Domain.Users;

public record FirstName(string Value);