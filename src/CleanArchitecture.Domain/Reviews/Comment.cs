﻿namespace CleanArchitecture.Domain.Reviews;

public record Comment(string Value);