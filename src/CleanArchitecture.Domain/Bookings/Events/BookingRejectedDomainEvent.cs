﻿using CleanArchitecture.Domain.Abstractions;

namespace CleanArchitecture.Domain.Bookings.Events;

public record BookingRejectedDomainEvent(Guid BookingId) : IDomainEvent;