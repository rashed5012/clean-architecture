﻿using CleanArchitecture.Domain.Abstractions;

namespace CleanArchitecture.Domain.Bookings.Events;

public record BookingCancelledDomainEvent(Guid BookingId) : IDomainEvent;