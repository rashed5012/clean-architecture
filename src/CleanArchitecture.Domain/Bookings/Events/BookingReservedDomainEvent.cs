﻿using CleanArchitecture.Domain.Abstractions;

namespace CleanArchitecture.Domain.Bookings.Events;

public record BookingReservedDomainEvent(Guid BookingId) : IDomainEvent;