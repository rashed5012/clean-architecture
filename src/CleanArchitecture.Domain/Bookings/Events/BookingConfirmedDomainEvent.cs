﻿using CleanArchitecture.Domain.Abstractions;

namespace CleanArchitecture.Domain.Bookings.Events;

public record BookingConfirmedDomainEvent(Guid BookingId) : IDomainEvent;