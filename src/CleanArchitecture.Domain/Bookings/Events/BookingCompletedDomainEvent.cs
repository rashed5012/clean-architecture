﻿using CleanArchitecture.Domain.Abstractions;

namespace CleanArchitecture.Domain.Bookings.Events;

public record BookingCompletedDomainEvent(Guid BookingId) : IDomainEvent;