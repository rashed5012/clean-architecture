﻿using CleanArchitecture.Domain.Apartments;
using CleanArchitecture.Domain.Shared;

namespace CleanArchitecture.Domain.Bookings;

public record PricingDetails(
    Money PriceForPeriod,
    Money CleaningFee,
    Money AmenitiesUpChange,
    Money TotalPrice);
    
