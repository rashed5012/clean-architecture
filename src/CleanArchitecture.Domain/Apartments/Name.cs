﻿namespace CleanArchitecture.Domain.Apartments;

public record Name(string Value);