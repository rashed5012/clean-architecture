﻿namespace CleanArchitecture.Domain.Apartments;

public record Description(string Value);